//
//  SimpleFactory_MantouProduct.swift
//  StartSwiftProject
//
//  Created by 侯海棠 on 2021/3/29.
//  Copyright © 2021 Monkey_Hou. All rights reserved.
//

import UIKit

class SimpleFactory_MantouProduct: SimpleFactory_Product {
    override func creatProduct() {
        self.product = "馒头"
    }
}
