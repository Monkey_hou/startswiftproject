//
//  SimpleFactory.swift
//  StartSwiftProject
//
//  Created by 侯海棠 on 2021/3/29.
//  Copyright © 2021 Monkey_Hou. All rights reserved.
//

import UIKit

class SimpleFactory: NSObject {
    static func makeProduct(type:String) -> SimpleFactory_Product {
        var product : SimpleFactory_Product?
        if type == "youtiao" {
            product = SimpleFactory_YoutiaoProduct()
        }else{
            product = SimpleFactory_MantouProduct()
        }
        return product!
    }
}
