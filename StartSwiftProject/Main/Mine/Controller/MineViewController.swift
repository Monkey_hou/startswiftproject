//
//  MineViewController.swift
//  StartSwiftProject
//
//  Created by 侯海棠 on 2019/7/22.
//  Copyright © 2019 Monkey_Hou. All rights reserved.
//

import UIKit

class MineViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        
        let textview = UILabel(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 100))
        self.view.addSubview(textview)
        textview.backgroundColor = .white
        textview.attributedText = "注册登录即代表同意".my_Attributed(attributed: { (make) in
            make.font()(UIFont.systemFont(ofSize: 18.0)).foregroundColor()(UIColor.black).kern()(3)
            make.append()("用户隐私协议").font()(UIFont.systemFont(ofSize: 18.0)).foregroundColor()(UIColor.blue).kern()(3)
            make.merge()
            
        })
        
        let factory = SimpleFactory.makeProduct(type: "youtiao")
        factory.creatProduct()
        print(factory.product)
        
        // Do any additional setup after loading the view.
    }
}
