//
//  Const.swift
//  StartSwiftProject
//
//  Created by 侯海棠 on 2019/7/21.
//  Copyright © 2019年 Monkey_Hou. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher
import RxSwift
import RxCocoa

//屏幕宽度
let screenWidth = UIScreen.main.bounds.width
//屏幕高度
let screenHeight = UIScreen.main.bounds.height
//服务器地址
let BaseUrl = "https://is.snssdk.com"
let QRBaseUrl = "https://api.qirexiaoshuo.com/"
let QMBaseUrl = "http://183.237.64.58:805"
let device_id: Int = 6096495334
let iid: Int = 5034850950

let newsTitleHeight: CGFloat = 40
let kMyHeaderViewHeight: CGFloat = 280
let kUserDetailHeaderBGImageViewHeight: CGFloat = 146

/// - Parameters:
///   - color_vaule: 传入0x5B5B5B格式的色值
///   - alpha: 传入透明度
/// - Returns: 颜色
func UIColorFromRGB(color_vaule : UInt64 , alpha : CGFloat = 1) -> UIColor {
    let redValue = CGFloat((color_vaule & 0xFF0000) >> 16)/255.0
    let greenValue = CGFloat((color_vaule & 0xFF00) >> 8)/255.0
    let blueValue = CGFloat(color_vaule & 0xFF)/255.0
    return UIColor(red: redValue, green: greenValue, blue: blueValue, alpha: alpha)
}


