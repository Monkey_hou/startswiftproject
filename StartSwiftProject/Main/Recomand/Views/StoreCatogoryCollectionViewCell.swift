//
//  StoreCatogoryCollectionViewCell.swift
//  StartSwiftProject
//
//  Created by 侯海棠 on 2019/8/3.
//  Copyright © 2019 Monkey_Hou. All rights reserved.
//

import UIKit

class StoreCatogoryCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var cellImage: UIImageView!
    
    @IBOutlet weak var cellTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setCellData(index: Int) -> Void {
        
        switch index {
        case 0:
          self.cellImage.image = UIImage(named:"store_home_catogary_boy")
          self.cellTitle.text = "男生"
        case 1:
            self.cellImage.image = UIImage(named:"store_home_catogary_girl")
            self.cellTitle.text = "女生"
        case 2:
            self.cellImage.image = UIImage(named:"store_home_catogary_free")
            self.cellTitle.text = "收藏"
        default:
            self.cellImage.image = UIImage(named:"store_home_catogary_rand")
            self.cellTitle.text = "排行"
        }
        
        
    }

}
