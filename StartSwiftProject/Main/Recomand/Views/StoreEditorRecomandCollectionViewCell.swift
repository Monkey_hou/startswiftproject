//
//  StoreEditorRecomandCollectionViewCell.swift
//  StartSwiftProject
//
//  Created by 侯海棠 on 2019/8/3.
//  Copyright © 2019 Monkey_Hou. All rights reserved.
//

import UIKit

class StoreEditorRecomandCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var bookImage: UIImageView!
    @IBOutlet weak var bookName: UILabel!
    @IBOutlet weak var authorName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setEditorCellData(editorData:BookStoreEditorRecomandBookModel) ->Void {
        
        // 最为简单的使用方式
        let url = URL(string:editorData.thumb ?? "");
        self.bookImage.kf.setImage(with: url)
        self.bookName.text = editorData.name
        self.authorName.text = editorData.author
    }

}
