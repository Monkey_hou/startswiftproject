//
//  BookStoreModel.swift
//  StartSwiftProject
//
//  Created by 侯海棠 on 2019/8/1.
//  Copyright © 2019 Monkey_Hou. All rights reserved.
//

import Foundation
//编辑推荐小说信息模型
class BookStoreEditorRecomandBookModel : HandyJSON{
    var id: String?
    var name: String?
    var author: String?
    var thumb: String?
    var type: String?
    var title: String?
    var bigthumb: String?
    
    required init() {}

}
//编辑推荐
class BookStoreEditorRecomandModel : HandyJSON{
    
    var data: [BookStoreEditorRecomandBookModel]?
    var moreId:String?
    var name :String?
    var moreCount :String?
    required init() {}
    
}

class BookStoreModel:HandyJSON {
    
    var goingData: BookStoreEditorRecomandModel?
    var today_recom: Dictionary<String, Any>?
    var closeData: BookStoreEditorRecomandModel?
    
    required init() {}
}
