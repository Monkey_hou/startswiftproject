//
//  BookStoreCollectionViewController.swift
//  StartSwiftProject
//
//  Created by 侯海棠 on 2019/8/3.
//  Copyright © 2019 Monkey_Hou. All rights reserved.
//

import UIKit

private let catogaryCellIdentifier = "StoreCatogoryCollectionViewCell"
private let editorRecomandCellIdentifier = "StoreEditorRecomandCollectionViewCell"
private let finishCellIdentifier = "StoreFinishedCollectionViewCell"
private let headerIdentifier = "headerIdentifier"
private let footerIdentifier = "footerIdentifier"
class BookStoreCollectionViewController: UICollectionViewController,UICollectionViewDelegateFlowLayout {
    
    var datasource: BookStoreModel?
    
    
    //一定要在初始化这里 给collection view赋值一个layout对象，否则会崩溃
    init(){
        super.init(collectionViewLayout:UICollectionViewFlowLayout.init())
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionView.backgroundColor = .white
        self.collectionView.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight)
        // 注册cell
        self.collectionView.register(UINib.init(nibName: "StoreCatogoryCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: catogaryCellIdentifier)
        
        self.collectionView.register(UINib.init(nibName: "StoreEditorRecomandCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: editorRecomandCellIdentifier)
       
        self.collectionView.register(UINib.init(nibName: "StoreFinishedCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: finishCellIdentifier)
        
        self.collectionView.register(CollectionHeader.self, forSupplementaryViewOfKind:UICollectionView.elementKindSectionHeader, withReuseIdentifier: headerIdentifier)
//        self.collectionView.register(UIView.self, forSupplementaryViewOfKind:UICollectionView.elementKindSectionFooter, withReuseIdentifier: footerIdentifier)
        

    }
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 3
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 4
        case 1:
            return self.datasource?.goingData?.data?.count ?? 0
        default:
            return self.datasource?.closeData?.data?.count ?? 0
        }
        
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch indexPath.section {
            case 0:
                let cell:StoreCatogoryCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: catogaryCellIdentifier, for: indexPath) as! StoreCatogoryCollectionViewCell
                cell.setCellData(index: indexPath.row)
                return cell
            case 1:
                let cell:StoreEditorRecomandCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: editorRecomandCellIdentifier, for: indexPath) as! StoreEditorRecomandCollectionViewCell
                //再赋值前作判断 避免请求完成之前加载视图因没有数据而崩溃
                if let bookRecomand = self.datasource?.goingData {
                    cell.setEditorCellData(editorData: (bookRecomand.data![indexPath.row]))
                    }
                return cell
            default:
                let cell:StoreFinishedCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: finishCellIdentifier, for: indexPath) as! StoreFinishedCollectionViewCell
                //再赋值前作判断 避免请求完成之前加载视图因没有数据而崩溃
                if let bookRecomand = self.datasource?.closeData {
                    cell.setEditorCellData(editorData: (bookRecomand.data![indexPath.row]))
                }
                return cell
        }
        
    }
    
    //根据每个区设置设置cell和视图边的间距
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {

        switch section {
        case 0:
            return UIEdgeInsets(top: 0, left: 10, bottom: 10,right: 10)
        case 1:
            return UIEdgeInsets(top: 0, left: 10, bottom: 10,right: 10)
        default:
            return UIEdgeInsets(top: 0, left: 0, bottom: 10,right: 0)

        }

    }
    
    //每个section中不同的行之间的行间距
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 10
        
    }
    //每个item之间的间距
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {

        return 10

    }
    
    //设置item的大小
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        
        switch indexPath.section {
        case 0:
            let itemH = 80
            let itemW = (screenWidth-50)/4
            return CGSize(width: Int(itemW), height: itemH)
            
        case 1:
            let itemH = 147
            let itemW = (screenWidth-40)/3
            return CGSize(width: Int(itemW), height: itemH)
            
        default:
            let itemH = 100
            let itemW = screenWidth
            return CGSize(width: Int(itemW), height: itemH)
        }
        
    }
    
    //collectionView 的点击事件
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    //设置HeadView的宽高

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize{
        
        if section == 0{
            return CGSize(width: screenWidth, height: 0)
        }else{
            return CGSize(width: screenWidth, height: 50)
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
//        if kind == UICollectionView.elementKindSectionHeader{
//
        let header : CollectionHeader = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: headerIdentifier, for: indexPath) as! CollectionHeader
        header.backgroundColor = UIColorFromRGB(color_vaule: 0xededed)
        
        switch indexPath.section {
        case 1:
            header.setHeaderTitle(title:self.datasource?.goingData?.name ?? "")
        case 2:
            header.setHeaderTitle(title:self.datasource?.closeData?.name ?? "")
        default:
            header.setHeaderTitle(title:self.datasource?.closeData?.name ?? "")
        }
        return header
//        }
    }
    //设置Footer的宽高
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize{
//
//        return CGSize(width: screenWidth, height: 50)
//
//    }

}
extension BookStoreCollectionViewController{
    
    func fetchData(storeData: BookStoreModel){
        
        self.datasource = storeData
        self.collectionView.reloadData()
    }

}

class CollectionHeader: UICollectionReusableView {
    var label: UILabel = {
        let label = UILabel()
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame);
        self.backgroundColor = UIColor.white;
        self.label.frame = CGRect(x: 15, y: 0, width:
            screenWidth-15, height: self.frame.size.height);
        self.addSubview(self.label);
        }
    func setHeaderTitle(title: String) -> Void {
        self.label.text = title
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder) has not been implemented")
        }
    
}

