//
//  RecomandViewController.swift
//  StartSwiftProject
//
//  Created by 侯海棠 on 2019/7/22.
//  Copyright © 2019 Monkey_Hou. All rights reserved.
//

import UIKit

class RecomandViewController: UIViewController {

    lazy var sotoreCollectionVC : BookStoreCollectionViewController = {
        let sotoreCollectionVC = BookStoreCollectionViewController()
        return sotoreCollectionVC
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .gray
        self.addChild(self.sotoreCollectionVC)
        self.view.addSubview(self.sotoreCollectionVC.collectionView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        BookStoreRequestTool.loadBookStoreData { (bookData:[BookStoreModel]) in
            
            self.sotoreCollectionVC.fetchData(storeData: bookData.last!)
        }
    }
    


}
