//
//  SearchModel.swift
//  StartSwiftProject
//
//  Created by 侯海棠 on 2019/7/29.
//  Copyright © 2019 Monkey_Hou. All rights reserved.
//

import Foundation

struct SearchModel : HandyJSON
{
    var id = ""
    var name = ""
}
