//
//  NewsModel.swift
//  StartSwiftProject
//
//  Created by 侯海棠 on 2019/7/28.
//  Copyright © 2019 Monkey_Hou. All rights reserved.
//

import Foundation

struct NewsModel :HandyJSON {
    var allvisit = 0
    var author = ""
    var cnum = 0
    var end = 0
    var id = 0
    var lastCharpterName = ""
    var mdate = ""
    var mmtime = ""
    var name = ""
    var mtime = ""
    var notReadCount = 0
    var rec_info = ""
    var rec_thumb = ""
    var rec_title = ""
    var thumb = ""
    var title = ""
    var vip = 0
    
}


