//
//  HomeRequestTool.swift
//  StartSwiftProject
//
//  Created by 侯海棠 on 2019/7/23.
//  Copyright © 2019 Monkey_Hou. All rights reserved.
//

import Foundation
import Alamofire
//Swift 中的protocol 所有方法都必须被实现，不存在@optional 这样的概念。为了实现可选接口有两个办法：（一）@objc 、（二）协议扩展.通过提供protocol的extension,我们为protocol提供了默认实现，这相当于“变相”将protocol中的方法设定为了optional
protocol HomeRequestToolProtocol {
    //static 修饰的静态方法不能被重写
    //这里强制要求加下划线是因为函数类型的参数不要带参数名称, 尤其是不能带外部参数名.
    //static 修饰的方法叫做静态方法，class 修饰的叫做类方法
    //在class里使用class关键字，而在struct或enum中仍然使用static
    static func
        loadHomeNewsTitleData(completionHandler:@escaping( _ newsTitles:[HomeNewsTitle])->Void)
    
    //获取推荐数据
    static func loadRecomandData(completionHandler:@escaping([NewsModel]) ->Void)
    
    //搜索页搜索数据
    static func loadSearchData(key:String , completionHandler:@escaping(_ results: [SearchModel])->Void);
    
}

extension HomeRequestToolProtocol {
    
    //获取首页头部标题
    static func loadHomeNewsTitleData(completionHandler:@escaping( _ newsTitles:[HomeNewsTitle])->Void){
  

        let url = BaseUrl + "/article/category/get_subscribed/v1/?"
        let params = ["device_id": device_id,
                      "iid": iid]
        Alamofire.request(url, parameters: params).responseJSON { (response) in
            // 网络错误的提示信息
            //如果guard后面的表达式求值false，guard则执行else代码块内的语句
            guard(response.result.isSuccess) else{return}
            if let value = response.result.value
            {
                //JSON时SwiftyJSON的方法
                let json = JSON(value)
                guard json["message"] == "success" else{return}
                //dictionary为JSON的扩展方法
                if let dataDict = json["data"].dictionary{

                    if let datas = dataDict["data"]?.arrayObject{
                        //初始化一个空数组 里面类型为HomeNewsTitle
                        var titles = [HomeNewsTitle]()
                        //这里如果不强制展开 加！ 会一直提醒 加可选默认值
                        titles.append(HomeNewsTitle.deserialize(from: "{\"category\": \"\", \"name\": \"推荐\"}")!)
                        titles += datas.compactMap({HomeNewsTitle.deserialize(from: $0 as? Dictionary)})
                        completionHandler(titles)
                    }
                }
            }
        }
    }

    //获取推荐数据
    static func loadRecomandData(completionHandler:@escaping(_ dataArr:[NewsModel]) ->Void){
        let url = QRBaseUrl+"iosBooks/v21_recommendBooks"
        let params:Dictionary = ["version":"1.2","page":1,"all":1,"sex":"1"] as [String : Any]
        Alamofire.request(url, parameters: params).responseJSON{
            (responce)->Void in

            guard (responce.result.isSuccess != false) else {return}
            if let value = responce.result.value{
                //JSON时SwiftyJSON的方法,先把返回的数据转为json
                let json = JSON(value)
                guard json["msg"] == "success" else {return}
                //再取出数据
                if let dataArr =  json["data"].arrayObject{
                    //初始化一个装数据模型的容器 可变数组
                    var modelArr = [NewsModel]()
                    modelArr += dataArr.compactMap({
                        //这里就是把data数组里面的元素 转成模型
                        NewsModel.deserialize(from: $0 as? Dictionary)
                    })
                    completionHandler(modelArr)
                }
            }
        }
    }

    //获取推荐数据
    static func loadSearchData(key:String , completionHandler:@escaping(_ results: [SearchModel])->Void){

//        let url = QRBaseUrl+"iosBooks/v21_auto_search"
//        let params:Dictionary = ["key":key] as [String : Any]
//        AF.request(url, parameters: params).responseJSON{
//            (responce)->Void in
//            guard (responce.result.success != nil) else {return}
//            if let value = responce.result.success{
//                //JSON时SwiftyJSON的方法,先把返回的数据转为json
//                let json = JSON(value)
//                guard json["status"] == 1 else {return}
////                //再取出数据
//                if let dataDic = json.dictionary{
//                    if let dataArr =  dataDic["key"]?.arrayObject{
//                        //初始化一个装数据模型的容器 可变数组
//                        var modelArr = [SearchModel]()
//                        modelArr += dataArr.compactMap({
//                            //这里就是把data数组里面的元素 转成模型
//                            SearchModel.deserialize(from: $0 as? Dictionary)
//                        })
//                        completionHandler(modelArr)
//                    }
//                }
//            }
//        }
    }

    
}

struct HomeRequestTool: HomeRequestToolProtocol {}
