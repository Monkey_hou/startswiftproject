//
//  SearchHistoryTool.swift
//  StartSwiftProject
//
//  Created by 侯海棠 on 2019/7/30.
//  Copyright © 2019 Monkey_Hou. All rights reserved.
//

import UIKit

//用来保存搜索历史的单例
class SearchHistoryTool: NSObject {
    
    var searchHistoryArr = [Dictionary<String, Any>]()
    static let `default` = SearchHistoryTool()

    @objc func saveSearchHistoryToDefault()->Bool{
        
        let userDefault = UserDefaults.standard
        userDefault.set(self.searchHistoryArr, forKey:"SearchHistory")
        if userDefault.synchronize() {
            
            print("save successe")
            return true
        }else{
            print("save defeat")
            return false
        }
        
    }
    
    @objc func obtainAllHistory()->Void{
        
        let userDefault = UserDefaults.standard
        let searchArr = userDefault.array(forKey:"SearchHistory")
        self.searchHistoryArr = searchArr as? [Dictionary<String, Any>] ?? []
        
    }
    
    @objc func saveSearchHistory(bookid:String,bookName:String) -> Void{
        
        let historyDic = ["bookid":bookid,"bookName":bookName]
        self.searchHistoryArr.append(historyDic)
        print("save successed")
        
    }
    @objc func removeHistoryWithIndex(index:Int) -> Void{
        
        self.searchHistoryArr.remove(at: index)
    }
    
    

}
