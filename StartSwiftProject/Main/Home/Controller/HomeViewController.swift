//
//  HomeViewController.swift
//  StartSwiftProject
//
//  Created by 侯海棠 on 2019/7/21.
//  Copyright © 2019年 Monkey_Hou. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
   
    private var pageTitleView : SGPageTitleView? = nil
    private var pageContentView: SGPageContentScrollView? = nil
    // 自定义导航栏
    //private lazy var navigationBar = HomeViewController.loadViewFromNib()

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        //设置状态栏属性
        navigationController?.navigationBar.barStyle = .black
        navigationController?.setNavigationBarHidden(false, animated: true)
        //设置导航栏右边按钮
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "搜索", style: .plain, target: self, action: #selector(navRightAction))
        
    }
    
    @objc func navRightAction() -> Void {
        
        self.navigationController?.pushViewController(SearchViewController(), animated: true)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
  
        setUI()
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

//设置子视图
extension HomeViewController
{
    private func setUI() -> Void {
    HomeRequestTool.loadHomeNewsTitleData(completionHandler:{(newsTitles:[HomeNewsTitle])->Void
            in
            let configuration = SGPageTitleViewConfigure()
            configuration.titleColor = .black
            configuration.titleSelectedColor = .globalRedColor()
            configuration.indicatorColor = .clear
            // 标题名称的数组
            self.pageTitleView = SGPageTitleView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: newsTitleHeight), delegate: self, titleNames: newsTitles.compactMap({$0.name}), configure: configuration)
            self.pageTitleView!.backgroundColor = .clear
            self.view.addSubview(self.pageTitleView!)
        
            var childVCs = [UIViewController]()

        _ = newsTitles.compactMap({ (newsTitle) -> () in
                switch newsTitle.category{
                    case .photos:
                        let imageVC = HomeTableViewController()
                        childVCs.append(imageVC)
                    
                        break
                    case .video:
                        let mineVC = MineViewController()
                        childVCs.append(mineVC)
                    
                    case .recommend:
                        let recomandVC = HomeTableViewController()
                        childVCs.append(recomandVC)
                case.hot:
                        let hotVC = HotViewController()
                        childVCs.append(hotVC)
                    default:
                        let recomandVC = HomeTableViewController()
                        childVCs.append(recomandVC)
                }
            })

            self.pageContentView = SGPageContentScrollView(frame: CGRect(x: 0, y: newsTitleHeight, width: screenWidth, height: self.view.height - newsTitleHeight), parentVC: self, childVCs:childVCs)
            self.pageContentView?.delegateScrollView = self
            self.view.addSubview(self.pageContentView!)
        })
    }
    
}


//在扩展实现代理方法 可读性更高
extension HomeViewController:  SGPageTitleViewDelegate, SGPageContentScrollViewDelegate
{
    //联动 pageContent 的方法
    func pageTitleView(pageTitleView: SGPageTitleView, index: Int) {
        
        self.pageContentView!.setPageContentScrollView(index: index)
        print("****************\(index)")
    }
    // 联动 SGPageTitleView 的方法
    func pageContentScrollView(pageContentScrollView: SGPageContentScrollView, progress: CGFloat, originalIndex: Int, targetIndex: Int) {
        
        self.pageTitleView!.setPageTitleView(progress: progress, originalIndex: originalIndex, targetIndex: targetIndex)
        print("----------------\(targetIndex)")
        
    }
    
//    func pageContentScrollView(pageContentScrollView: SGPageContentScrollView, index: Int) {
//        if index == 1 || index == 5 {
//            pageTitleView?.removeBadge(index: index)
//        }
//    }
    
    
}
