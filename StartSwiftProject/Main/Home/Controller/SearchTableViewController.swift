//
//  SearchTableViewController.swift
//  StartSwiftProject
//
//  Created by 侯海棠 on 2019/7/29.
//  Copyright © 2019 Monkey_Hou. All rights reserved.
//

import UIKit


class SearchTableViewController: UITableViewController {

    var dataSource = [SearchModel]()
    let searchCellIdentifier :String = "SearchTableViewCell"

    override func viewDidLoad() {
        super.viewDidLoad()
        //读取NIB文件
        let nib = UINib(nibName:"SearchTableViewCell",bundle:nil)
        //通过nib注册cell
        self.tableView.register(nib, forCellReuseIdentifier: searchCellIdentifier)
        self.tableView.rowHeight = 50

    }
    
    @objc func fetchTabelData(key:String ) -> Void{
        HomeRequestTool.loadSearchData(key: key, completionHandler: {(results:[SearchModel]) -> Void
            in
            self.dataSource += results
            self.tableView.reloadData()
        })
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1;
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.dataSource.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.cellForRow(at: indexPath) as? SearchTableViewCell
        if cell == nil{
            cell = tableView.dequeueReusableCell(withIdentifier:self.searchCellIdentifier) as? SearchTableViewCell
        }
        cell?.setCellData(cellModel:self.dataSource[indexPath.row])
        return cell!
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let searchModel = self.dataSource[indexPath.row] 
        SearchHistoryTool.default.saveSearchHistory(bookid: searchModel.id, bookName: searchModel.name)
        
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return setHeaderView()
    }
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
}

//对外开放的接口不要放在扩展里面
extension SearchTableViewController
{
    //表格区头
    @objc func setHeaderView()->UIView{
        let headLabel = UILabel(frame: CGRect(x: 15, y: 0, width: screenWidth, height: 50))
        headLabel.text = "搜索结果:\(self.dataSource.count)条"
        headLabel.textColor = .red
        headLabel.font = .boldSystemFont(ofSize: 20.0)
        return headLabel
    }
    
}
