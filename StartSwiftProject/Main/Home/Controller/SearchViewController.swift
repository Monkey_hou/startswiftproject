//
//  SearchViewController.swift
//  StartSwiftProject
//
//  Created by 侯海棠 on 2019/7/29.
//  Copyright © 2019 Monkey_Hou. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController {

    lazy var searchBar :UISearchBar? = nil
    lazy var searchTabel : SearchTableViewController? = nil
    //懒加载对象的定义 要实现初始化等操作的话 在下面代码块里面进行就可以了
    lazy var searchHistoryTabel:SearchHistoryTableViewController? = {
        var searchHistoryTabel = SearchHistoryTableViewController()
        searchHistoryTabel = SearchHistoryTableViewController()
        searchHistoryTabel.tableView.frame = CGRect(x:0, y: 0, width: screenWidth, height: screenHeight)
        return searchHistoryTabel
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColorFromRGB(color_vaule: 0xededed)
        //设置导航栏搜索框
        setNavSearchBar()
        setSearchTabelVC()
        setSearchHistoryTabel()
    }
    
    
}

//设置搜索结果表格 搜索历史表格
extension SearchViewController
{
    @objc func setSearchTabelVC()->Void{
        self.searchTabel = SearchTableViewController()
        self.searchTabel?.tableView.isHidden = true
        searchTabel?.tableView.frame = CGRect(x:0, y: 0, width: screenWidth, height: screenHeight)
        self.addChild(self.searchTabel!)
        self.view.addSubview(self.searchTabel!.tableView)
    }
    
    @objc func setSearchHistoryTabel()->Void{
        
        self.addChild(self.searchHistoryTabel!)
        self.view.addSubview(self.searchHistoryTabel!.tableView)
        
    }
    
}


//设置搜索框
extension SearchViewController:UISearchBarDelegate{
    
    @objc func setNavSearchBar()->Void{
        
        let searchView : UIView = UIView(frame:CGRect(x:0,y:0,width:screenWidth-150,height:30))
        self.searchBar = UISearchBar(frame:CGRect(x:0,y:0,width:searchView.width,height:30))
        self.searchBar?.delegate = self as UISearchBarDelegate
        searchView.addSubview(self.searchBar!)
        self.navigationItem.titleView = searchView
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "取消", style: .plain, target: self, action: #selector(cancelAction))
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        
        
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        
        if text.count == 0 {
            self.searchTabel?.tableView.isHidden = true;
            
            return true
        }
   //第一个参数，被替换字符串的range，第二个参数，即将键入或者粘贴的string，返回的是改变过后的新str，即textfield的新的文本内容
        let checkStr = (searchBar.text! as NSString).replacingCharacters(in: range, with: text)
        if (checkStr.count >= 100) {
            return false;
        }
        self.searchTabel?.fetchTabelData(key: checkStr)
        self.searchTabel?.tableView.isHidden = false
        self.view.bringSubviewToFront(self.searchTabel!.tableView)
        return true
    }
    
    @objc func cancelAction()->Void{
        
        self.searchBar?.resignFirstResponder()
        self.searchTabel?.tableView.isHidden = true
    }
    
    
}
