//
//  HomeTableViewController.swift
//  StartSwiftProject
//
//  Created by 侯海棠 on 2019/7/27.
//  Copyright © 2019 Monkey_Hou. All rights reserved.
//

import UIKit

let cellIdentifier:String = "HomeRecomandTableViewCell"

class HomeTableViewController: UITableViewController {
    
    var dataSource = [NewsModel]()
    var manager: ZJTableViewManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColorFromRGB(color_vaule: 0xededed)

        /********动态表格********/
        self.manager = ZJTableViewManager(tableView: self.tableView)
        //xib布局 一定要记得动态高的label行数设置为0 否则动态算高会失效
        self.manager.register(HomeRecomandTableViewCell.self, AutomaticHeightCellItem.self)
        
        /********动态表格********/
        
        //读取NIB文件
//                let nib = UINib(nibName:"HomeRecomandTableViewCell",bundle:nil)
//                //通过nib注册cell
//                self.tableView.register(nib, forCellReuseIdentifier: cellIdentifier)
         fetchData()
    }
    
    private func fetchData(){
        
        HomeRequestTool.loadRecomandData { (dataArr:[NewsModel]) in

            self.dataSource += dataArr
//            self.tableView.reloadData()
//
            /********动态表格********/
            //add section
            let section = ZJTableViewSection()
            self.manager.add(section: section)
            
            for newsModel in self.dataSource {
                let item = AutomaticHeightCellItem()
                item.newsModel = newsModel
                //计算高度
                item.autoHeight(self.manager)
                section.add(item: item)
            }
            self.manager.reload()
            /********动态表格********/
        }
        
    }

    // MARK: - Table view data source
//
//    override func numberOfSections(in tableView: UITableView) -> Int {
//
//        return 1
//    }
//
//    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//
//        return self.dataSource.count
//    }
//
//    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//
//        var cell = tableView.cellForRow(at: indexPath) as? HomeRecomandTableViewCell
//        if cell == nil{
//            cell = tableView.dequeueReusableCell(withIdentifier:cellIdentifier) as? HomeRecomandTableViewCell
//        }
//
//        cell?.setCellData(newsModel: self.dataSource[indexPath.row] )
//
//        return cell!
//    }
}


