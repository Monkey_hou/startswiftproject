//
//  HotViewController.swift
//  StartSwiftProject
//
//  Created by 侯海棠 on 2019/7/31.
//  Copyright © 2019 Monkey_Hou. All rights reserved.
//

import UIKit

class HotViewController: UIViewController {

    let identifier = "HotCollectionViewCell"
    let margin = 10
    var dataSource: [NewsModel] = []
    
    //懒加载
    lazy var hotCollectView : UICollectionView = {
        
        let layout = UICollectionViewFlowLayout.init()
        layout.itemSize = CGSize(width: (Int(screenWidth) - 4*self.margin)/3, height: 140)
        layout.minimumLineSpacing = CGFloat(self.margin)
        layout.minimumInteritemSpacing = CGFloat(self.margin)
        layout.scrollDirection = .vertical
        layout.sectionInset = UIEdgeInsets.init(top: 5, left: 10, bottom: 5, right: 10)
        // 设置分区头视图和尾视图宽高
//        layout.headerReferenceSize = CGSize.init(width: screenWidth, height: 80)
//        layout.footerReferenceSize = CGSize.init(width: screenWidth, height: 80)
        let hotCollectView = UICollectionView.init(frame: self.view.bounds, collectionViewLayout:layout)
        hotCollectView.backgroundColor = UIColor.white
        hotCollectView.delegate = self
        hotCollectView.dataSource = self
        return hotCollectView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addSubview(self.hotCollectView)
        // 注册cell
        self.hotCollectView.register(UINib.init(nibName: "HotCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: identifier)
        fetchData()
        // 注册headerView
//        collectionView?.register(CollectionHeaderView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: headerIdentifier)
//        // 注册footView
//        collectionView?.register(CollectionFootView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: footIdentifier)
        
    }
    
    @objc func fetchData()->Void{
        
        HomeRequestTool.loadRecomandData { (dataArr:[NewsModel]) in
            
            self.dataSource += dataArr
            self.hotCollectView.reloadData()
        }
        
    }
 
}

extension HotViewController :UICollectionViewDelegate,UICollectionViewDataSource{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:HotCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as! HotCollectionViewCell
        cell.setCellData(cellData:self.dataSource[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath.row)
    }
    
    
}
