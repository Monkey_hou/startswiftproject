//
//  HotCollectionViewCell.swift
//  StartSwiftProject
//
//  Created by 侯海棠 on 2019/7/31.
//  Copyright © 2019 Monkey_Hou. All rights reserved.
//

import UIKit

class HotCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var bookImage: UIImageView!
    @IBOutlet weak var bookAuthor: UILabel!
    @IBOutlet weak var bookName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setCellData(cellData: NewsModel) -> Void {
        
        // 最为简单的使用方式
        let url = URL(string:cellData.thumb);
        self.bookImage.kf.setImage(with: url)
        
        //self.headImage.image = UIImage(named: item.newsModel.thumb)
        self.bookName.text = cellData.rec_info
        self.bookAuthor.text = cellData.author
        
    }

}
