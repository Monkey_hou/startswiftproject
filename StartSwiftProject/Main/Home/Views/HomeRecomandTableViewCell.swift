//
//  HomeRecomandTableViewCell.swift
//  StartSwiftProject
//
//  Created by 侯海棠 on 2019/7/28.
//  Copyright © 2019 Monkey_Hou. All rights reserved.
//

import UIKit

class AutomaticHeightCellItem: ZJTableViewItem {
    var newsModel : NewsModel!
}

//动态高度的cell说明：支持系统autolayout搭建的cell（xib以及snapkit等基于autolayout的约束框架理论上都是支持的）
class HomeRecomandTableViewCell: ZJTableViewCell {
    @IBOutlet weak var headImage: UIImageView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
//    public func setCellData(newsModel:NewsModel) -> Void {
//        self.titleLabel.text = newsModel.rec_info
//        self.timeLabel.text = newsModel.author
//        self.contentLabel.text = newsModel.title
//        //kingfisher的使用
//        // 最为简单的使用方式
//        //let url = URL(string:newsModel.thumb);
//
//    }
    
    /// cell即将出现在屏幕中的回调方法 在这个方法里面赋值
    override func cellWillAppear() {
        let item = self.item as! AutomaticHeightCellItem
        
        // 最为简单的使用方式
        let url = URL(string:item.newsModel.thumb);
        self.headImage.kf.setImage(with: url)
        
        //self.headImage.image = UIImage(named: item.newsModel.thumb)
        self.titleLabel.text = item.newsModel.rec_info
        self.timeLabel.text = item.newsModel.author
        self.contentLabel.text = item.newsModel.title
    }

    @IBAction func btnAction(_ sender: UIButton) {
        
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
