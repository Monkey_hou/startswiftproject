//
//  SearchTableViewCell.swift
//  StartSwiftProject
//
//  Created by 侯海棠 on 2019/7/29.
//  Copyright © 2019 Monkey_Hou. All rights reserved.
//

import UIKit

class SearchTableViewCell: UITableViewCell {
    @IBOutlet weak var contentLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    public func setCellData(cellModel:SearchModel) -> Void {
        
        self.contentLabel.text = cellModel.name
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
