//
//  VidioTableViewCell.swift
//  StartSwiftProject
//
//  Created by 侯海棠 on 2019/8/4.
//  Copyright © 2019 Monkey_Hou. All rights reserved.
//

import UIKit

class VidioTableViewCell: UITableViewCell {
    @IBOutlet weak var bookImage: UIImageView!
    @IBOutlet weak var bookName: UILabel!
    @IBOutlet weak var bookDate: UILabel!
    @IBOutlet weak var bookAuthor: UILabel!
    @IBOutlet weak var bookLastCharpter: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setCellData(cellData:NewsModel) -> Void {
        
        // 最为简单的使用方式
        let url = URL(string:cellData.thumb);
        self.bookImage.kf.setImage(with: url)
        //self.headImage.image = UIImage(named: item.newsModel.thumb)
        self.bookName.text = cellData.rec_info
        self.bookDate.text = cellData.mdate
        self.bookAuthor.text = cellData.author
        self.bookLastCharpter.text = cellData.lastCharpterName
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
