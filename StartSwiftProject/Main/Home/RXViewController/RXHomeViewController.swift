//
//  RXHomeViewController.swift
//  StartSwiftProject
//
//  Created by jimi on 2020/3/30.
//  Copyright © 2020 Monkey_Hou. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Moya
class RXHomeViewController: UIViewController {

    let cellIdentifier = "RXHomeTableViewCell"
    let provider = MoyaProvider<RXHomeAPIManager>()
    
    let dataSource = RXHomeViewModel()
    //rxswift资源回收
    let dispostBag = DisposeBag()
    lazy var mainTabel: UITableView = {
        let tabelView = UITableView()
        //tabelView.separatorStyle = UITableViewCell.SeparatorStyle.none
        tabelView.rowHeight = 60.0
        tabelView.backgroundColor = UIColorFromRGB(color_vaule:0xF2F4F8)
        let nib = UINib(nibName:"RXHomeTableViewCell",bundle:nil)
        tabelView.register(nib, forCellReuseIdentifier: cellIdentifier)
        return tabelView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addSubview(self.mainTabel)
        self.mainTabel.frame = self.view.frame
        
        provider.request(.getDeviceListRequest){result in
            if case let .success(response) = result {
                //解析数据
                let data = try? response.mapJSON()
                let json = JSON(data!)
                
                //刷新表格数据
                DispatchQueue.main.async{
                }
            }
            
        }
        
        
        dataSource.data.bind(to: mainTabel.rx.items(cellIdentifier: cellIdentifier,cellType: RXHomeTableViewCell.self)){_,model,cell in
            cell.nameLabel.text = model.name
            cell.ageLabel.text = model.age
            cell.dateLabel.text = model.birthDay
        }.disposed(by: dispostBag
        )
        mainTabel.rx.modelSelected(RXHomeModel.self).subscribe(onNext: {model in
            print("点击了\(model.name)")
        }).disposed(by: dispostBag)
        
        mainTabel.rx.itemSelected.subscribe(onNext: {indexPath in
            print("点击了第\(indexPath.row)行")
        }, onError: { error in
            
        }, onCompleted: {
            
        }, onDisposed: {
            
        })
    }
    
}
