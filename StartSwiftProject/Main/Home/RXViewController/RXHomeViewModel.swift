//
//  RXHomeViewModel.swift
//  StartSwiftProject
//
//  Created by jimi on 2020/3/30.
//  Copyright © 2020 Monkey_Hou. All rights reserved.
//

import UIKit
import RxSwift
struct RXHomeViewModel {
    let data = Observable.just([
        RXHomeModel(name: "jack", age: "18", birthDay: "2000-10-11"),
        RXHomeModel(name: "rose", age: "22", birthDay: "2000-10-11"),
        RXHomeModel(name: "jimi", age: "15", birthDay: "2000-10-11"),
        RXHomeModel(name: "jonse", age: "18", birthDay: "2000-10-11"),
        RXHomeModel(name: "liunix", age: "30", birthDay: "2000-10-11")
    ])
    
}
