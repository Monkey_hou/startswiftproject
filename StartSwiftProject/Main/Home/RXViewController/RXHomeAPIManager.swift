//
//  RXHomeAPIManager.swift
//  StartSwiftProject
//
//  Created by jimi on 2020/4/14.
//  Copyright © 2020 Monkey_Hou. All rights reserved.
//

import UIKit
import Moya
import Foundation
//是否生产环境
let ISPRODUCTION = true
//前面后台正式环境---后面后台测试环境
let JMBseUrl = ISPRODUCTION ? "http://60.220.220.233:8890" : "http://183.237.64.58:805"
enum RXHomeAPIManager{
    //获取设备列表
    case getDeviceListRequest
    //更新宝贝资料
    case updateBabyImfomation(String,String)
}

extension RXHomeAPIManager: TargetType {
    var baseURL: URL {
        return  URL.init(string: JMBseUrl)!
    }
    
    var path: String {
        return "/api"
    }
    
    var method: Moya.Method {
        return .post
    }
    
    var sampleData: Data {
        return "".data(using: String.Encoding.utf8)!
    }
    
    var task: Task {
        switch self {
        case .getDeviceListRequest:
            var params: [String: Any] = [:]
                params["ver"] = "1"
                params["token"] = "dfc2d17be47c66fa345691ebb7342d4c"
                params["sign"] = "32E151AB7C295230C7DE205CA85CA00F"
                params["method"] = "queryBindEquipmentList"
            return .requestParameters(parameters: params,
            encoding: URLEncoding.default)
        case .updateBabyImfomation(let name, let sex):
            var params: [String: Any] = [:]
                params["ver"] = "1"
                params["token"] = "dfc2d17be47c66fa345691ebb7342d4c"
                params["sign"] = "32E151AB7C295230C7DE205CA85CA00F"
                params["method"] = "queryBindEquipmentList"
            return .requestParameters(parameters: params,
            encoding: URLEncoding.default)
        default:
            return .requestPlain
        }
    }
    
    var headers: [String : String]? {
        return ["Content-type": "application/json"]
    }
    var validate: Bool {
        return false
    }
}
