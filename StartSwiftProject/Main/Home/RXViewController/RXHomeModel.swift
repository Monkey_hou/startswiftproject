//
//  RXHomeModel.swift
//  StartSwiftProject
//
//  Created by jimi on 2020/3/30.
//  Copyright © 2020 Monkey_Hou. All rights reserved.
//

import UIKit
import Foundation
class RXHomeModel: Codable {
    var name = ""
    var age = ""
    var birthDay = ""
    init(name:String,age:String,birthDay:String){
        self.name = name
        self.age = age
        self.birthDay = birthDay
    }
}
