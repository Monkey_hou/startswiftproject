//
//  RXHomeTableViewCell.swift
//  StartSwiftProject
//
//  Created by jimi on 2020/3/30.
//  Copyright © 2020 Monkey_Hou. All rights reserved.
//

import UIKit

class RXHomeTableViewCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
