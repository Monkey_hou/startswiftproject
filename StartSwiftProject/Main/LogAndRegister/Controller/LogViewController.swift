//
//  LogViewController.swift
//  StartSwiftProject
//
//  Created by 侯海棠 on 2019/7/27.
//  Copyright © 2019 Monkey_Hou. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class LogViewController: UIViewController {
    
    @IBOutlet weak var accountTextfiled: UITextField!
    
    @IBOutlet weak var passwordTextfiled: UITextField!
    
    @IBOutlet weak var logBtn: UIButton!
    
    @IBOutlet weak var registerBtn: UIButton!
    
    @IBOutlet weak var forgetPassword: UIButton!
    
    @IBAction func LogAction(_ sender: UIButton) {

        //获取app delegate对象
        let appdelegate = (UIApplication.shared.delegate) as! AppDelegate
        appdelegate.window?.rootViewController = BaseTabBarController()

    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //清除包DisposeBag,功能类似于arc，管理订阅的生命周期
        let disposeBag = DisposeBag()
        
        let userNameValid = accountTextfiled.rx.text.orEmpty.asObservable()
        userNameValid.map({$0.count >= 5 })
        userNameValid.share(replay: 1)
////        .share(replay: 1, scope: .forever)
//
        let passWordValid = passwordTextfiled.rx.text.orEmpty.asObservable()
        passWordValid.map({$0.count >= 5})
        passWordValid.share(replay: 1)
        
        //Observable 可以用于描述元素异步产生的序列。
        //combineLatest 方法，将用户名是否有效和密码是否有效合并成两者是否同时有效。然后用这个合成后来的序列来控制按钮是否可点击。
        
//        let bouthValid = Observable.combineLatest(
//            passWordValid,
//            userNameValid
//        ){$0 && $1}
//        let bouthValid = Observable.combineLatest(
//            passWordValid,
//            userNameValid
//        ){$0 && $1}
//        .share(replay: 1)
        
        //将可监听序列绑定到观察者上：
//        userNameValid.bind(to:passwordTextfiled.rx.isEnabled)
//        .disposed(by: disposeBag)
        
//        passWordValid.bind(to:passwordTextfiled.rx.isEnabled)
//        .disposed(by: disposeBag)
        
//        bouthValid.bind(to: logBtn.rx.controlEvent(.touchUpInside))
//            .disposed(by: disposeBag)
        
        //观察者 是用来监听事件，然后它需要这个事件做出响应.响应事件的都是观察者
        //btn的观察者框架已经帮我们创建好了
        //创建观察者最直接的方法就是在 Observable 的 subscribe 方法后面描述，事件发生时，需要如何做出响应。而观察者就是由后面的 onNext，onError，onCompleted的这些闭包构建出来的。
//        logBtn.rx.tap.subscribe(onNext: { [weak self] in
//            self?.showAlert()
//        }, onError: { error in
//                print("发生错误： \(error.localizedDescription)")
//        }, onCompleted: {
//            print("任务完成")
//        })
//        .disposed(by: disposeBag)

        // 按钮点击序列
        let taps: Observable<Void> = self.logBtn.rx.controlEvent(.touchUpInside).asObservable()
        // 每次点击后弹出提示框
        taps.subscribe(onNext: { [weak self] in
            self?.showAlert()
        })
        .disposed(by: disposeBag)
        
       
        
    }


    func showAlert() {
        let alertView = UIAlertView(
            title: "提示",
            message: "账号和密码为空",
            delegate: nil,
            cancelButtonTitle: "OK"
        )
        alertView.show()
        
        let appdelegate = (UIApplication.shared.delegate) as! AppDelegate
        appdelegate.window?.rootViewController = BaseTabBarController()
    }
}
