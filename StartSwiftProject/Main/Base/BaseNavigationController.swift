//
//  BaseNavigationController.swift
//  StartSwiftProject
//
//  Created by 侯海棠 on 2019/7/22.
//  Copyright © 2019 Monkey_Hou. All rights reserved.
//

import UIKit

class BaseNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //关闭导航栏半透明效果
        self.navigationBar.isTranslucent = false
        self.navigationBar.tintColor = UIColor.red
        //导航栏背景色
        self.navigationBar.barTintColor = UIColorFromRGB(color_vaule: 0xededed)
        //UIColor(red: 255.0 / 255, green: 122.0 / 255, blue: 139.0 / 255, alpha: 1.0)
        //设置标题颜色
        let dic:NSDictionary =  [NSAttributedString.Key.foregroundColor: UIColor.red,NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 18)]
        
        //1.as的使用场合从派生类转换为基类，向上转类型(upcasting)
//as?和as!操作符的转换规则是一样的，只是as?在转换失败之后会返回nil对象，转换成功之后返回一个可选类型(optional)，需要我们拆包使用。
        //由于as?转换失败也不会报错，所以对于能够100%确定使用as!能够转换成功的，使用as!,否则使用as?
        self.navigationBar.titleTextAttributes = dic as? [NSAttributedString.Key : AnyObject]
        
        
    }
    
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        
        if viewControllers.count>0 {
            
            viewController.hidesBottomBarWhenPushed = true
            viewController.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"lefterbackicon_titlebar_24x24_"), style: .plain, target: self, action: #selector(navigationBack))
            
        }
        //一定要调用父类的压入方法 否则无法压入控制器,也会导致tabbar的按钮无法显示
        super.pushViewController(viewController, animated: true)
    }
    
    /// 重写此方法让 preferredStatusBarStyle 响应
    override var childForStatusBarStyle: UIViewController? {
        return self.topViewController
    }
    //@objc自动清除冗余代码减小包大小,在编译完成后可以检测出没有被调用到的swift 函数，优化删除后可以减小最后二进制文件的大小
    @objc private func navigationBack(){
        popViewController(animated: true)
    }
    
}
