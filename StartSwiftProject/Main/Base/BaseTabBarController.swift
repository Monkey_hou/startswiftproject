//
//  BaseTabBarController.swift
//  StartSwiftProject
//
//  Created by 侯海棠 on 2019/7/22.
//  Copyright © 2019 Monkey_Hou. All rights reserved.
//

import UIKit

class BaseTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        tabBar.barTintColor = UIColorFromRGB(color_vaule: 0xededed)
        tabBar.tintColor = UIColor.red //tabbar 字体颜色
        tabBar.barTintColor = UIColor.white
        addChildViewControllers()
    }
    
    private func addChildViewControllers(){
        
        setChildVC(vc: RXHomeViewController(), image: "home", title: "首页")
        setChildVC(vc: DiscoverViewController(), image: "huoshan", title: "书架")
        setChildVC(vc: RecomandViewController(), image: "video", title: "书城")
        setChildVC(vc: MineViewController(), image: "mine", title: "我的")
        
    }
    
    private func setChildVC(vc:UIViewController,image:String,title:String){
        
        vc.tabBarItem.image = UIImage(named: image + "_tabbar")
        vc.tabBarItem.selectedImage = UIImage(named: image + "_tabbar_press")
        vc.tabBarItem.title = title
        vc.tabBarItem.setTitleTextAttributes(NSDictionary.init(dictionary: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 15)]) as? [NSAttributedString.Key : Any], for: UIControl.State.normal)
        vc.title = title
        addChild(BaseNavigationController(rootViewController:vc))
        
    }
    
}
