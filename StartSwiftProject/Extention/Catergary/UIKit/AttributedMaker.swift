//
//  AttributedMaker.swift
//  StartSwiftProject
//
//  Created by 侯海棠 on 2021/3/22.
//  Copyright © 2021 Monkey_Hou. All rights reserved.
//

import UIKit
//链式编程封装：核心就是要返回调用者对象本身，达到循环调用自己的目的
class AttributedMaker: NSObject {

     var stringArr : Array<String> = []
     var attributedStringArr : Array<NSMutableAttributedString>? = []
    private var my_lineSpacing : Float?
    private var my_lineBreakMode : NSLineBreakMode?
    private var my_alignment : NSTextAlignment?
}

extension AttributedMaker{
    //设置font
    func font() -> ((UIFont)->AttributedMaker) {
        return { (font) -> AttributedMaker in
            let string = self.stringArr.last
            let attributedString = self.attributedStringArr?.last
            attributedString?.addAttribute(NSAttributedString.Key.font, value: font, range: NSRange.init(location: 0, length: string?.count ?? 0))
            return self
        }
    }
    //设置文字颜色
    func foregroundColor() -> ((UIColor) -> AttributedMaker) {
        return { (color) -> AttributedMaker in
            let string = self.stringArr.last
            let attributedString = self.attributedStringArr?.last
            attributedString?.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: NSRange.init(location: 0, length: string?.count ?? 0))
            return self
        }
    }
    //设置背景颜色
    func backgroundColor() -> ((UIColor) -> AttributedMaker) {
        return {(color) -> AttributedMaker in
            let string = self.stringArr.last
            let attributedString = self.attributedStringArr?.last
            attributedString?.addAttribute(NSAttributedString.Key.backgroundColor, value: color, range: NSRange.init(location: 0, length: string?.count ?? 0))
            return self
        }
    }
    //删除线高度
    func strikethroughStyle() -> ((NSInteger) -> AttributedMaker) {
        return {(value) -> AttributedMaker in
            let string = self.stringArr.last
            let attributedString = self.attributedStringArr?.last
            attributedString?.addAttribute(NSAttributedString.Key.backgroundColor, value: value, range: NSRange.init(location: 0, length: string?.count ?? 0))
            return self
        }
    }
    //字间距
    func kern() -> ((Float) -> AttributedMaker) {
        return {(value) -> AttributedMaker in
            let string = self.stringArr.last
            let attributedString = self.attributedStringArr?.last
            attributedString?.addAttribute(NSAttributedString.Key.kern, value: value, range: NSRange.init(location: 0, length: string?.count ?? 0))
            return self
        }
    }
    //行间距
    func lineSpacing() -> ((CGFloat) -> AttributedMaker) {
        return {(value) -> AttributedMaker in
            let string = self.stringArr.last
            let attributedString = self.attributedStringArr?.last
            let style = NSMutableParagraphStyle()
            style.lineSpacing = value
            self.my_lineSpacing = Float(value)
            if (self.my_lineBreakMode != nil){
                style.lineBreakMode = self.my_lineBreakMode!
            }
            if (self.my_alignment != nil){
                style.alignment = self.my_alignment!
            }
            attributedString?.addAttribute(NSAttributedString.Key.paragraphStyle, value: style, range: NSRange.init(location: 0, length: string?.count ?? 0))
            return self
        }
    }
    //对齐方式
    func textAlignment() -> ((NSTextAlignment) -> AttributedMaker) {
        return {(value) -> AttributedMaker in
            let string = self.stringArr.last
            let attributedString = self.attributedStringArr?.last
            let style = NSMutableParagraphStyle()
            style.alignment = value
            self.my_alignment = value
            if (self.my_lineSpacing != nil){
                style.lineSpacing = CGFloat(self.my_lineSpacing!)
            }
            if (self.my_lineBreakMode != nil){
                style.lineBreakMode = self.my_lineBreakMode!
            }
            attributedString?.addAttribute(NSAttributedString.Key.paragraphStyle, value: style, range: NSRange.init(location: 0, length: string?.count ?? 0))
            return self
        }
    }
    //追加文字
    func  append() -> ((String) -> AttributedMaker) {
        return {(text) -> AttributedMaker in
            self.stringArr.append(text)
            self.attributedStringArr?.append(NSMutableAttributedString(string: text))
            return self
        }
    }
    //合并（例如追加完文字以后，来个总设置）
    func merge() -> AttributedMaker {
        var string = ""
        self.stringArr.forEach { (objc) in
            string += objc
        }
        self.stringArr.removeAll()
        self.stringArr.append(string)
        
        let attributedString = NSMutableAttributedString()
        self.attributedStringArr?.forEach({ (attStr) in
            attributedString.append(attStr)
        })
        self.attributedStringArr?.removeAll()
        self.attributedStringArr?.append(attributedString)
        
        return self
    }
    
}
