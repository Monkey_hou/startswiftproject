//
//  String+my_Attributed.swift
//  StartSwiftProject
//
//  Created by 侯海棠 on 2021/3/22.
//  Copyright © 2021 Monkey_Hou. All rights reserved.
//

import Foundation

extension String{
    func my_Attributed(attributed: (_ make: AttributedMaker) ->()) -> NSMutableAttributedString {
        let make = AttributedMaker()
        make.stringArr.append(self)
        make.attributedStringArr?.append(NSMutableAttributedString(string: self))
        attributed(make)
        
        let attributedString = NSMutableAttributedString()
        make.attributedStringArr?.forEach({ (objc) in
            attributedString.append(objc)
        })
        return attributedString
    }
}
